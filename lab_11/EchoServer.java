import java.net.Socket;
import java.net.ServerSocket;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
* A basic EchoServer that recieves aninterger vaule from a client,
* sleeps for that number of seconds, then echoes tbe Integer
* back to the client.
* @author Silvino Pina
* @version 17 April 2018
*/
public class EchoServer{
  public static void main(String[] args) throws Exception{
    // Create our ServerSocket.
    System.out.println("Creating server socket....");
    int port = 8083;
    ServerSocket server = new ServerSocket(port);

    // Await an incoming connection.
    while(true){
      // Accepts an incoming connection.
      System.out.println("Awaiting connection from the client.....");
      Socket client = server.accept();
      System.out.println("Connection received !");
      InputStream input = client.getInputStream();
      ObjectInputStream objectInput = new ObjectInputStream(input);

      // Receive an Integer from client.
      int sleepTime = (Integer) objectInput.readObject();
      System.out.println("Recieved " + sleepTime + " from client.");

      // Sleep for that numed of secods.
      System.out.println("Sleeping for " + sleepTime + " seconds.");
      Thread.sleep(sleepTime * 1000);

      // Echo the Integer back to the client.
      System.out.println("Waking. Echoing back to client.");
      OutputStream output = client.getOutputStream();
      ObjectOutputStream objectOutput = new ObjectOutputStream(output);
      objectOutput.writeObject(sleepTime);

      // Clean up streams and sockets.
      System.out.println("Cleaning up streams and socket.");
      objectOutput.flush();
      objectOutput.close();
      objectInput.close();
      client.close();
    }
  }
}
