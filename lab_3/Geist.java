/**
* An adapter that wraps class <code>Ghost</code>
* and converst it to a german language interface.
* @author Silvino Pina
* @version 6 February 2018
*/

public class Geist{
	private Ghost adaptee;

	public Geist(String name){
		adaptee = new Ghost(name); 
	}

	public void erschrecken(){
		adaptee.scare();
	}

	public void herumgeistern(){
		adaptee.haunt();
	}
}