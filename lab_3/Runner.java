/*8
* A quicl test class for our two design pattern 
* examples for the day
* @author Silvino Pina
*@version 6 Feburary 2018
*/


public class Runner{
	public static void main(String[] args){
		TheOneRing trueRing = TheOneRing.getInstance();
		TheOneRing copyRing = TheOneRing.getInstance();

		trueRing.castIntoTheFire();
		copyRing.castIntoTheFire();

		// Check for identity.
		System.out.println ((trueRing == copyRing) ? "Yayy!" : "Whoops...");

		Geist germanGhost = new Geist("Hans");
		germanGhost.erschrecken();
		germanGhost.herumgeistern();
	}

}