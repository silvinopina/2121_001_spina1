import java.util.NoSuchElementException;
/**
* A basic queue implementation build on a circular array
* Demonstrates the difficulties of combinin arrays and
* generics in java, and illustrates an array-based implementation
* of a dat structure previously only seen in a linked
* implementation
* @author Silvino Pina
* @version 10 April 2018
*/
public class Queue<T>{
  private T[] array;
  private int head;   //index of first elemnet to remove
  private int tail;   //index of the first empty spot of the array
  private int size;

@SuppressWarnings("unchecked")
  public Queue(){
    this.size = 5;
    this.head = 0;
    this.tail = 0;
    /*
    * The followin g line produces the unchecked warning
    * The contract for Queue will ensure that only objections
    * of type T will be added to the array.
    */
    this.array = (T[])new Object[this.size];
  }//end Queue method


/**
* Adds a new elemnet to the end of the queue.
* @param element the item to add
*/
public void enqueue(T element){
  if (this.isFull()){
    this.resizeArray();
  }

  this.array[this.tail] = element;
  this.tail = (this.tail + 1) % this.size;
}//end enqueue method


/**
* Returns the element at the head of the queue.
* @reutrn the head of the Queue
* @thorws NoSuchElementException when the queue is empty
*/
public T dequeue() throws NoSuchElementException{
  if (this.isEmpty()){
    throw new NoSuchElementException("Queue is empty.");
  }
  T temp = this.array[this.head];
  this.head = (this.head + 1) % this.size;
  return temp;
}



 private boolean isEmpty(){
    return (this.head == this.tail);
}//end is Empty method


  private boolean isFull(){
      return ((this.tail + 1)%this.size) == this.head;
  }//end isFull method

@SuppressWarnings("unchecked")
  private void resizeArray(){
    /*
    * The followin g line produces the unchecked warning
    * The warning is suppressed because of reasoning
    * similar to the above
    */
    T[] temp = (T[]) new Object[this.size * 2];
    int numberCopied = 0;   //index into new arrays

    for(int i = this.head; i != this.tail; i = (i + 1) % this.size){
      temp[numberCopied] = this.array[i];
      numberCopied++;
    }

    this.array = temp;
    this.head = 0;
    this.tail = numberCopied;
    this.size *= 2;
  }//end resizeArray method
}//end Queue Class
