import java.util.Arrays;
import java.util.LinkedList;
import java.util.ListIterator;
/**
* A quick program to illustrate three different ways of iterating
* through a LinkedList and one of the pitfalls of using an
* Iterator
*
* @author Silvino Pina
* @version 13 March 2018
*/

public class IntegerList{
  public static void main(String[] args) {
      Integer[] numbers = {7, 2, 5, 9, 4, 10, 21,
                          31, 6, 19, 2, 32, 21};
      LinkedList<Integer> data = new LinkedList<Integer>(
        Arrays.asList(numbers));

      System.out.println();
      System.out.println("Processing with for loop: ");
      for (int i = 0; i < data.size(); i++ ) {
        if (data.get(i) % 2 ==0) {
          System.out.println("Removing: " + data.get(i));
          data.remove(i);
          i--;
        }
        else{
          System.out.println(data.get(i));
        }
      }

      System.out.println();
      System.out.println("Processing with Iterator while loops: ");
      data = new LinkedList<Integer>(Arrays.asList(numbers));

      ListIterator<Integer> iterator = data.listIterator(0);
      while (iterator.hasNext()){
        int temp = iterator.next();
        if (temp % 2 == 0){
          System.out.println("Removing; " + temp);
          iterator.remove();
        }
        else {
          System.out.println(temp);
        }
      }

      System.out.println();
      System.out.println("Processing with enhanced for loop: ");

      data = new LinkedList<Integer>(Arrays.asList(numbers));

      for(Integer element : data){
          if(element % 2 == 0 ){
            System.out.println("Removing: "+ element);
            data.remove(element);
          }
          else{
            System.out.println(element);
          }
      }
  }
}
