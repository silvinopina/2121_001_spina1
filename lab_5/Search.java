/**
* A quick implementation of a linear search that illustrastes
*  the difference between iterative amd recursive strategies
* @author Silvino Pina
* @version 27 February 2018
*/

public class Search{
	public static int find(int key, int[] array){
		for(int i = 0; i < array.length; i++){
			if(key == array[i]){
				return i;
			}
		}
		return -1;
	}

	public static int findRecursively(int key, int[] array){
		return findRecursively(key, array, 0);

	}

	private static int findRecursively(int key, int[] array, int index){
		//Nothing left to search 
		if(index == array.length){
			return -1;
		}
		// Found a match
		else if (key == array[index]){
			return index;
		}
		//Keep Looking
		else{
			return findRecursively(key, array, index + 1);
		}
	}

}